<?php
/**
 * Created by PhpStorm.
 * User: terminator10
 * Date: 09/15/18
 * Time: 08:04 PM
 */
namespace App;

use Illuminate\Database\Eloquent\Model;


class Rent extends Model{

    protected $fillable = [
        'user_id',
        'barbecue_id',
    ];

    public function user(){

        return $this->belongsTo(User::class);
    }

    public function barbecue(){

        return $this->belongsTo(Barbecue::class);
    }

}