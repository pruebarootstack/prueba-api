<?php
/**
 * Created by PhpStorm.
 * User: terminator10
 * Date: 09/15/18
 * Time: 07:30 PM
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class Barbecue extends Model{

    const AVAILABLE = true;
    const NOT_AVAILABLE = false;

    protected $fillable = [
        'name',
        'description',
        'model',
        'cost',
        'image',
        'latitude',
        'longitude',
        'available'
    ];

    public function rents(){

        return $this->hasMany(Rent::class);
    }

}
