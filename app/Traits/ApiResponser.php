<?php
/**
 * Created by PhpStorm.
 * User: terminator10
 * Date: 09/15/18
 * Time: 06:58 PM
 */

namespace App\Traits;

trait ApiResponser{


    private function successResponse($code = 200, $message, $data = null){

        return response()->json([
            'statusCode' => $code,
            'message' => $message,
            'data' => $data
        ]);
    }

    private function errorResponse($code, $message){

        return response()->json([
            'statusCode' => $code,
            'message' => $message
        ]);
    }


}