<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return "Hola mundo";
});

//USER
$router->post('auth/login', ['uses' => 'AuthController@authenticate']);
$router->post('auth/register', ['uses' => 'AuthController@signUp']);

//BARBECUE
$router->group(['middleware' => 'jwt.auth'], function () use ($router) {
    $router->get('barbecues', ['uses' => 'BarbecueController@index']);

    $router->post('barbecuelatlong', ['uses' => 'BarbecueController@getBarbecueLatLong']);

    $router->get('barbecues/{barbecue}', ['uses' => 'BarbecueController@show']);

    $router->get('rents/{barbecue}/{user}', ['uses' => 'RentController@rentBarbecue']);
});