<?php

use Illuminate\Database\Seeder;

class BarbecuesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Barbecue::class, 20)->create();
    }
}
