<?php
/**
 * Created by PhpStorm.
 * User: terminator10
 * Date: 09/15/18
 * Time: 08:44 PM
 */
namespace App\Http\Controllers;

use App\Traits\ApiResponser;
use App\User;
use Firebase\JWT\JWT;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller {


    use ApiResponser;

    protected function jwt(User $user){

        $payload = [
            "iss" => "lumen-jwt",
            "sub" => $user->id,
            "iat" => time(),
            "exp" => time() + 60*60
        ];

        return JWT::encode($payload, env('JWT_SECRET'));
    }

    public function authenticate(\Illuminate\Http\Request $request){

        $rules = [
            'email'     => 'required|email',
            'password'  => 'required'
        ];

        $this->validate($request, $rules);

        $user = User::where('email', $request->input('email'))->first();

        if (!$user){
            return $this->errorResponse(404, 'Email no existe. Intente nuevamente');
        }

        if (Hash::check($request->input('password'), $user->password)) {

            return $this->successResponse(200, 'Inicio sesion correctamente', ['token' => $this->jwt($user)]);
        }


        return $this->errorResponse(404, 'Revise su email o password');
    }

    public function signUp(\Illuminate\Http\Request $request){

        $rules = [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'zip_code' => 'required',
            'password' => 'required|min:8'
        ];

        $this->validate($request, $rules);

        $user = User::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'zip_code' => $request->input('zip_code'),
            'password' => Hash::make($request->password)
        ]);

        return $this->successResponse(201, 'Se registro correctamente', $this->jwt($user));

    }


}
