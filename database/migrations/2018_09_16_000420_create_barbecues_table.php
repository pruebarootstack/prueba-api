<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBarbecuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('barbecues', function (Blueprint $table) {
            $table->increments('id');
            $table->string('model')->unique();
            $table->string('name');
            $table->string('description');
            $table->float('cost');
            $table->string('image')->nullable();
            $table->double('latitude');
            $table->double('longitude');
            $table->boolean('available')->default(\App\Barbecue::NOT_AVAILABLE);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('barbecues');
    }
}
