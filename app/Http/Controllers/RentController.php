<?php
/**
 * Created by PhpStorm.
 * User: terminator10
 * Date: 09/15/18
 * Time: 10:35 PM
 */

namespace App\Http\Controllers;


use App\Barbecue;
use App\Rent;
use App\Traits\ApiResponser;
use App\User;
use Illuminate\Http\Request;

class RentController extends Controller{


    use ApiResponser;

    /*
     * Proceso para Rentar barbacoas
     *
     */

    public function rentBarbecue($barbecue, $user){

        $user = User::findOrFail($user);
        $barbecue = Barbecue::findOrFail($barbecue);

        if ($barbecue->available == Barbecue::NOT_AVAILABLE){

            return $this->errorResponse(404, 'Esta barbacoa no se encuentra disponible');
        }

        $rent = Rent::create([
            'user_id' => $user->id,
            'barbecue_id' => $barbecue->id
        ]);

        if ($rent){

            $barbecue->available = Barbecue::NOT_AVAILABLE;
            $barbecue->save();

            return $this->successResponse(200, 'Ha rentado la barbacoa', $rent);
        }

        return $this->errorResponse(500, 'Error al procesar la solicitud');

    }


}