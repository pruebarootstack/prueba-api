<?php
/**
 * Created by PhpStorm.
 * User: terminator10
 * Date: 09/15/18
 * Time: 10:12 PM
 */

namespace App\Http\Controllers;


use App\Barbecue;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;


class BarbecueController extends Controller{

    use ApiResponser;

    public function index(){

        $barbecues = Barbecue::all();

        return $this->successResponse(200, 'success', $barbecues);

    }

    public function show($id){

        $barbecue = Barbecue::findOrFail($id);
        return $barbecue;
    }

    public function getBarbecueLatLong(Request $request){

        $data = $this->calculatePosition($request->latitude, $request->longitude);

        return $this->successResponse(200, 'success', $data);

    }

    protected function calculatePosition($latitude, $longitude){

        $all = array();

        $km = 111.302;
        $degtorad = 0.01745329;
        $radtodeg = 57.29577951;

        $barbecues = Barbecue::all();

        foreach ($barbecues as $barbecue){

            $dlong = ($barbecue->longitude - $longitude);

            $dvalue = (sin($barbecue->latitude * $degtorad) * sin($latitude * $degtorad)) + (cos($barbecue->latitude * $degtorad) * cos($latitude * $degtorad) * cos($dlong * $degtorad));

            $dd = acos($dvalue) * $radtodeg;

            $result = round(($dd * $km), 2);

            if ($result <= 10){
                array_push($all, $barbecue);
            }
        }

        return $all;
    }

}