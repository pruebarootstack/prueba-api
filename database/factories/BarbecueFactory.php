<?php
/**
 * Created by PhpStorm.
 * User: terminator10
 * Date: 09/15/18
 * Time: 07:44 PM
 */

$factory->define(\App\Barbecue::class, function (Faker\Generator $faker) {

    return [
        'model' => $faker->ean8,
        'name' => $faker->name,
        'description' => $faker->text($maxNbChars = 100),
        'cost' => $faker->randomNumber(2),
        'image' => $faker->randomElement(['https://www.bricoreyes.es/media/catalog/product/cache/1/image/9df78eab33525d08d6e5fb8d27136e95/b/a/barbacoa-800-montana-char-broil-9.jpg', 'http://estag.fimagenes.com/img/4/t/W/v/tWv_900.jpg', 'http://tapasmagazine.es/app/uploads/2015/05/gtres_a00650091_231.jpg']),
        'latitude' => $faker->randomElement(['9.0076044', '9.0076044', '9.0047345', '9.0187817', '8.9824542', '9.104098']),
        'longitude' => $faker->randomElement(['-79.4637891', '-79.4637891', '-79.507375', '-79.5020224', '-79.5126512', '-79.3717914']),
        'available' => $faker->randomElement([\App\Barbecue::NOT_AVAILABLE, \App\Barbecue::AVAILABLE])
    ];

});